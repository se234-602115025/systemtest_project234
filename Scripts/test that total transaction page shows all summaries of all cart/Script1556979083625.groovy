import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.87.206.11:8081/')

WebUI.setText(findTestObject('TestTransactionPageShowAllSummaries/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('TestTransactionPageShowAllSummaries/input_Password_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('TestTransactionPageShowAllSummaries/button_Login'))

WebUI.click(findTestObject('TestTransactionPageShowAllSummaries/a_Total Transaction'))

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/th_1'), '1')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_1'), '1')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_Garden Papaya'), 'Garden, Papaya')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_20120 THB'), '20,120 THB')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/th_2'), '2')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_2'), '2')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_Banana Garden Banana Rambutan'), 'Banana, Garden, Banana, Rambutan')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_60570 THB'), '60,570 THB')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/th_3'), '3')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_3'), '3')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_Rambutan Banana'), 'Rambutan, Banana')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_170 THB'), '170 THB')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/th_4'), '4')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_4'), '4')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_Garden Banana'), 'Garden, Banana')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/td_100600 THB'), '100,600 THB')

WebUI.verifyElementText(findTestObject('TestTransactionPageShowAllSummaries/Total price summary'), 'Total price: 181,460 THB')

WebUI.closeBrowser()

