import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.87.206.11:8081/')

WebUI.setText(findTestObject('TestClickAddToCartObject/input_Username'), username)

WebUI.setEncryptedText(findTestObject('TestClickAddToCartObject/input_Password'), password)

WebUI.click(findTestObject('TestClickAddToCartObject/button_Login'))

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/h2_Products'), 'Products')

WebUI.click(findTestObject('TestClickAddToCartObject/button_add to cart01'))

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/button_add to cart01'), 'already added')

WebUI.click(findTestObject('TestClickAddToCartObject/button_add to cart02'))

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/button_add to cart02'), 'already added')

WebUI.click(findTestObject('TestClickAddToCartObject/button_add to cart03'))

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/button_add to cart03'), 'already added')

WebUI.click(findTestObject('TestClickAddToCartObject/button_add to cart04'))

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/button_add to cart04'), 'already added')

WebUI.click(findTestObject('TestClickAddToCartObject/button_add to cart05'))

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/button_add to cart05'), 'already added')

WebUI.verifyElementText(findTestObject('TestClickAddToCartObject/numberOfProduct'), '5')

WebUI.closeBrowser()

