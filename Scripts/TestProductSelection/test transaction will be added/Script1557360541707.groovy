import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.87.206.11:8081/')

WebUI.setText(findTestObject('TestProductionSelectionObject/input_Username'), 'admin')

WebUI.setEncryptedText(findTestObject('TestProductionSelectionObject/input_Password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('TestProductionSelectionObject/button_Login_1'))

WebUI.click(findTestObject('TestProductionSelectionObject/a_Total Transaction'))

'#4'
WebUI.verifyElementText(findTestObject('TestProductionSelectionObject/th_4'), '4')

'Transaction ID'
WebUI.verifyElementText(findTestObject('TestProductionSelectionObject/td_4'), '4')

WebUI.verifyElementText(findTestObject('TestProductionSelectionObject/td_Garden Banana'), 'Garden, Banana')

WebUI.verifyElementText(findTestObject('TestProductionSelectionObject/td_100600 THB'), '100,600 THB')

WebUI.closeBrowser()

