import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.87.206.11:8081/')

WebUI.setText(findTestObject('TestProductionSelectionObject/input_Username'), 'user')

WebUI.setEncryptedText(findTestObject('TestProductionSelectionObject/input_Password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('TestProductionSelectionObject/button_Login'))

'add Garden\r\n'
WebUI.click(findTestObject('TestProductionSelectionObject/button_add to cart_Garden'))

'add Banana\r\n'
WebUI.click(findTestObject('TestProductionSelectionObject/button_add to cart_Banana'))

WebUI.click(findTestObject('TestProductionSelectionObject/a_Carts'))

WebUI.setText(findTestObject('TestProductionSelectionObject/input_Garden_amount'), '5')

WebUI.setText(findTestObject('TestProductionSelectionObject/input_Banana_amount'), '4')

WebUI.click(findTestObject('TestProductionSelectionObject/button_confirm'))

WebUI.acceptAlert()

WebUI.verifyElementText(findTestObject('TestProductionSelectionObject/well done message'), 'Well done!')

WebUI.closeBrowser()

