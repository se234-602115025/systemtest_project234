import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.87.206.11:8081/')

WebUI.verifyElementText(findTestObject('TestLogoutObject/h2_Login'), 'Login')

WebUI.verifyElementText(findTestObject('TestLogoutObject/button_Login'), 'Login')

WebUI.setText(findTestObject('TestLogoutObject/input_Username'), username)

WebUI.setEncryptedText(findTestObject('TestLogoutObject/input_Password'), password)

WebUI.click(findTestObject('TestLogoutObject/button_Login'))

WebUI.verifyElementText(findTestObject('TestLogoutObject/h2_Products'), 'Products')

WebUI.verifyElementText(findTestObject('TestLogoutObject/button_Logout'), 'Logout')

WebUI.click(findTestObject('TestLogoutObject/button_Logout'))

WebUI.verifyElementText(findTestObject('TestLogoutObject/h2_Login'), 'Login')

WebUI.verifyElementText(findTestObject('TestLogoutObject/button_Login'), 'Login')

WebUI.closeBrowser()

