<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TestAll</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fab5bec2-07ee-4a77-a712-8dcfe4aec0df</testSuiteGuid>
   <testCaseLink>
      <guid>16c6a4b2-7bd9-49ca-8ffe-99bac7e77b1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test open web site then system bring to login page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c980ac0-e96b-480e-99f5-4a15a9d92a8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a80e2393-28aa-4807-b45f-057d9abcb12d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForLogin</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>a80e2393-28aa-4807-b45f-057d9abcb12d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>25ea4cc5-1037-496f-b25c-830fbc8eea75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a80e2393-28aa-4807-b45f-057d9abcb12d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>1f534247-2c63-4a3b-8f33-13258f4c9eab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>a80e2393-28aa-4807-b45f-057d9abcb12d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>transaction/cart</value>
         <variableId>9d1c6d58-4f7e-4676-a4c4-19243264a0d9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51521618-c86a-4f9c-8548-60919a65adb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestIncorrectLogin/Test if the password is not correct, the error message is shown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6516831e-e23f-43d5-84d1-9dabd5a1ac42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestIncorrectLogin/Test if the user name and password are not correct, the error message is shown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86b0f951-d999-4da2-804b-9bb395e40356</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestIncorrectLogin/Test if the user name is not correct, the error message is shown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01e60b35-f066-4304-bac3-8fa9fb4de23d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestEmptyInputBox/Test empty password input box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1a70852-7f13-4d28-a5ee-9f243a223f26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestEmptyInputBox/Test empty username input box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c625775c-ca03-4147-86a5-55eb0f722dec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestAllAvaiableProductAreShown/test all avaiable product are shown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ba48606-77cc-4b9c-996e-445272551001</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestAllAvaiableProductAreShown/test is in product page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6576931-6ea8-42f1-b9e7-664afaae03b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test click add to cart</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f409c34e-5655-4b73-96c1-31e9b58c72b0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForClickAddButton</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f409c34e-5655-4b73-96c1-31e9b58c72b0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>9e160531-6d01-47ce-b82d-40e8636a7a6c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f409c34e-5655-4b73-96c1-31e9b58c72b0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>80e95b0b-222a-41bd-8453-11ec917b25a1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>93112046-f2af-437d-8e16-8c7fcc7c5272</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestProductSelection/test user clicks confirm button and transaction will be added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6738d6c3-f577-45e2-925d-a6a43923e14f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestProductSelection/test after selecting the product all selected products are in cart page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>052ff185-3cfa-42b1-9585-c2449292da78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestProductSelection/test user can change the amount of products for each product type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6dc8b34-d649-4006-ae7e-4c556757760a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case logout</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>92ebd9b2-2f95-4091-a7a7-526f61a08003</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForLogout</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>92ebd9b2-2f95-4091-a7a7-526f61a08003</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>6900dee6-bb43-44c5-bc93-f305b2f8d410</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>92ebd9b2-2f95-4091-a7a7-526f61a08003</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>1573f338-fc23-468a-81a0-7e134c9aa5db</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>613a7bf6-f538-45d1-ae6a-edf541e9ce42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/test that total transaction page shows all summaries of all cart</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
