<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite click add to cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f1154de0-7d87-48d7-abcf-d024823457ec</testSuiteGuid>
   <testCaseLink>
      <guid>bfd9762d-47dc-40bf-a754-7e5d4ce771bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test click add to cart</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>86297de8-2be3-441f-9596-00121319fb18</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForClickAddButton</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>86297de8-2be3-441f-9596-00121319fb18</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>9e160531-6d01-47ce-b82d-40e8636a7a6c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>86297de8-2be3-441f-9596-00121319fb18</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>80e95b0b-222a-41bd-8453-11ec917b25a1</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
