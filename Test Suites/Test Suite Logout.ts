<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite Logout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>22183e23-22ad-4cc8-a7eb-6e30d747a7ff</testSuiteGuid>
   <testCaseLink>
      <guid>c50fe139-ec68-4df4-a034-a09ed6fff50f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test case logout</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ff8d3b11-7be4-4563-999d-59e6ae7122c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForLogout</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>ff8d3b11-7be4-4563-999d-59e6ae7122c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>6900dee6-bb43-44c5-bc93-f305b2f8d410</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ff8d3b11-7be4-4563-999d-59e6ae7122c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>1573f338-fc23-468a-81a0-7e134c9aa5db</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
