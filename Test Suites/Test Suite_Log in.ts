<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite_Log in</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>31c71a9e-fcff-4b27-abca-0b9f77ca263a</testSuiteGuid>
   <testCaseLink>
      <guid>8efa2421-12ef-4e23-8879-b339c7baf968</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Test login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>738560b5-ee96-4c04-85fe-e4d2241cf2da</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestDataForLogin</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>738560b5-ee96-4c04-85fe-e4d2241cf2da</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>25ea4cc5-1037-496f-b25c-830fbc8eea75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>738560b5-ee96-4c04-85fe-e4d2241cf2da</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>1f534247-2c63-4a3b-8f33-13258f4c9eab</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>738560b5-ee96-4c04-85fe-e4d2241cf2da</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>transaction/cart</value>
         <variableId>9d1c6d58-4f7e-4676-a4c4-19243264a0d9</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
