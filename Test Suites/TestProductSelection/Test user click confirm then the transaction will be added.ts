<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>After the user select the product than go to cart page and click confirm. The product will be add to transaction</description>
   <name>Test user click confirm then the transaction will be added</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f314da56-dd1a-446d-ac0f-3156a53d0a55</testSuiteGuid>
   <testCaseLink>
      <guid>6e6e87d4-3f93-45c5-b2bf-8e674d2a8c65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestProductSelection/test user clicks confirm button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e231466f-33ba-4bf0-9ed2-a7fa6ca5eb66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestProductSelection/test transaction will be added</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
